public class FOD11_method {
    public static String nama = "Hanas Setyani Putri";
    public static String getNama(){
        return nama;
    }
    public static int hitungUmur(int lahir, int sekarang){
        int umur = sekarang - lahir;
        return umur;
    }
    public static void main(String[] args){
        String namaSaya = getNama();
        System.out.println("Nama saya: "+namaSaya);
        int umurSaya = hitungUmur(1999, 2023);
        System.out.println("Umur saya: "+umurSaya);
    }
}