import java.util.Scanner;
public class FOD6_trycatch {
    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        try{
            int number;
            number = input.nextInt();
            if(number < 50)
                throw new Exception("value is to small");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("Continue after the catch block");
    }
}
