import java.util.Scanner;
import java.lang.System;
public class TK3_game {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String title = "Coepoe World Puzzle";
        System.out.println(title);
        for(int i = 0; i < title.length(); i++) {
            System.out.printf("=");
        }

        System.out.println();
        System.out.println("Rules :");
        System.out.println("1. Create a word using given characters, min 3 characters & max 6 characters.");
        System.out.println("2. Each level, You have 10 chances to answer correctly.");
        System.out.println("3. To get through to next level, you have to score 70 points each level.");

        char ulang;

        do{
            String lv1 = "level 1";
            System.out.println(lv1);
            for(int i = 0; i < lv1.length(); i++) {
                System.out.printf("-");
            }
            System.out.println();
            String[]answer1 = {"die", "led", "lei", "let", "lid", "lie", "lit", "tie", "deli", "diet", "edit", "idle", "lied", "tide", "tied", "tile", "tilt", "tilde", "tiled", "title", "tilted", "titled"};
            System.out.println("d e t t l i");

            int score = 0;
            for (int u = 1; u <= 10; u++){

                System.out.print("Your Answer :");
                String answer = input.nextLine();
                if (answer.length() < 3 || answer.length() > 6){
                    System.out.println("karakter yang dimasukkan min. 3 dan max. 6");
                }

                for (String i: answer1) {
                    if(i.equals(answer)) {
                        score += 10;
                        System.out.println("#Right. Score : " + score);
                    }
                }
            }

            System.out.println("You had answered 10 times with " + score/10 + " right answers..");

            int total_score = score;
            if (score >= 70 ){
                System.out.println("Corect answers :");
                for (String i: answer1) {
                    System.out.print(i + " ");
                }
                System.out.println();

                String lv2 = "level 2";
                System.out.println(lv2);
                for(int i = 0; i < lv2.length(); i++) {
                    System.out.printf("-");
                }
                System.out.println();
                String[]answer2 = {"sec", "can", "cane", "scan", "scene", "case", "cease" };
                System.out.println("s e c a e n");

                int score2 = 0;
                for (int u = 1; u <= 10; u++){

                    System.out.print("Your Answer :");
                    String answer = input.nextLine();
                    if (answer.length() < 3 || answer.length() > 6){
                        System.out.println("karakter yang dimasukkan min. 3 dan max. 6");
                    }

                    for (String i: answer2) {
                        if(i.equals(answer)) {
                            score2 += 10;
                            System.out.println("#Right. Score : " + score2);
                        }
                    }
                }

                System.out.println("You had answered 10 times with " + score2/10 + " right answers..");

                total_score += score2;
                if (score2 >= 70){

                    System.out.println("Corect answers :");
                    for (String i: answer2) {
                        System.out.print(i + " ");
                    }
                    System.out.println();

                    String lv3 = "level 3";
                    System.out.println(lv3);
                    for(int i = 0; i < lv3.length(); i++) {
                        System.out.printf("-");
                    }
                    System.out.println();
                    System.out.println("h k r n e o");
                    String[]answer3 = {"honk", "honker", "roe", "ore", "her", "hen", "one", "nor"};

                    int score3 = 0;
                    for (int u = 1; u <= 10; u++){

                        System.out.print("Your Answer :");
                        String answer = input.nextLine();
                        if (answer.length() < 3 || answer.length() > 6){
                            System.out.println("karakter yang dimasukkan min. 3 dan max. 6");
                        }

                        for (String i: answer3) {
                            if(i.equals(answer)) {
                                score3 += 10;
                                System.out.println("#Right. Score : " + score3);
                            }
                        }
                    }

                    System.out.println("You had answered 10 times with " + score3/10 + " right answers..");

                    total_score += score3;

                    if (score3 >= 70){
                        System.out.println("Corect answers :");
                        for (String i: answer3) {
                            System.out.print(i + " ");
                        }
                        System.out.println();

                        System.out.println("Overall score : " +total_score);
                        System.out.println("You win!!");
                        System.out.println("Press ENTER to exit..");
                        Scanner scanner = new Scanner(System.in);
                        scanner.nextLine();
                        System.exit(0);
                    }

                }
            }


            System.out.print("You lose!! Try again.. ");
            System.out.print("Do you want to retry (y/t)? ");
            ulang = input.next().charAt(0);

        } while ((ulang=='y') || (ulang =='Y'));

    }

}
