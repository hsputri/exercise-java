import java.util.Scanner;

class faktorial {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        int faktorial = 1;
        System.out.println("Masukkan bilangan bulat : ");
        int n = in.nextInt();
        if (n <= 0){
            System.out.println("Angka yang dimasukkan bukan bilangan bulat atau adalah angka 0");
        } else {
            for (int i = 1; i <= n; i++){
                faktorial *= i;
                System.out.print(i + " ");
                if(i<n){
                    System.out.print("* ");
                }
            }
            System.out.println("= "+ String.valueOf(faktorial));
        }

        in.close();
    }
}