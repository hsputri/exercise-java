import java.util.Scanner;

public class TP2 {
    public static void main(String args[]){
        Scanner input = new Scanner(System.in);

        char ulang;
        do {
            System.out.println("Belajar Deret Aritmatika, Geometri dan menghitung Faktorial");
            int a =1;
            int n;
            do {
                System.out.print("\nMasukkan Banyak Angka yang Ingin Dicetak [2...10] :");
                n = input.nextInt();
                if(n < 2 || n > 10){
                    System.out.println("angka yang dimasukkan min. 2 dan max. 10");
                }
            } while (n < 2 || n > 10);

            int b;
            do {
                System.out.print("\nMasukkan  Beda Masing-Masing Angka yang Diinput [2...9] :");
                b = input.nextInt();
                if(b < 2 || b > 9){
                    System.out.println("angka yang dimasukkan min. 2 dan max. 9");
                }
            } while (b < 2 || b > 9);

            System.out.println("Deret Aritmatika:");
            int i;
            for (i=1; i<=n; i++) {
                System.out.print(a+(i-a)*b + " ");
            }
            System.out.println();
            System.out.println("Deret Geometri:");

            for(int j = 1; j<=n; j++){
                System.out.print(a + " ");
                a = a * b;
            }
            System.out.println();

            int faktorial = 1;
            System.out.println("Faktorial dari " + n + ":");
            for (int k = 1; k <= n; k++){
                faktorial *= k;
                System.out.print(k + " ");
                if(k<n){
                    System.out.print("* ");
                }
            }
            System.out.println("= "+ String.valueOf(faktorial));

            System.out.print("Anda Ingin Mengulang (y/t)? ");
            ulang = input.next().charAt(0);

            System.out.println();
        } while ((ulang=='y') || (ulang =='Y'));
    }
}

