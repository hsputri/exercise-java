import java.util.Scanner;

public class FOD5_1 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.print("Masukkan Nama Mahasiswa :");
        String nama = input.nextLine();
        System.out.print("Masukkan Nilai Mahasiswa :");
        int nilai = input.nextInt();

        if (nilai > 70) {
            System.out.println("Mahasiswa bernama "+ nama +" dinyatakan LULUS dengan nilai "+ nilai);
        }else {
            System.out.println("Mahasiswa bernama " + nama +" dinyatakan TIDAK LULUS");
        }
    }
}
