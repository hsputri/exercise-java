import java.util.Scanner;
public class TP1_4 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        String string1 = "Selamat Datang di Program Ramalan Cupu";
        System.out.println(string1);
        for(int i = 0; i <= string1.length(); i++) {
            System.out.printf("+");
        }

        String string2 = "Data Anda :";
        System.out.println("\n" + string2);
        for(int i = 0; i < string2.length(); i++) {
            System.out.printf("\u2665");
        }
        System.out.println();
        System.out.print("Masukkan Nama Anda :");
        String nama1 = input.nextLine();
        System.out.print("Masukkan Umur Anda :");
        int umur1 = input.nextInt();
        input.nextLine();

        String string3 = "Data Pasangan Anda :";
        System.out.println("\n" + string3);
        for(int i = 0; i < string3.length(); i++) {
            System.out.printf("\u2665");
        }
        System.out.println();
        System.out.print("Masukkan Nama Pasangan Anda :");
        String nama2 = input.nextLine();
        System.out.print("Masukkan Umur Pasangan Anda :");
        int umur2 = input.nextInt();

        System.out.println("\n" + nama1 + " [" + umur1 + "] tahun");

//      Love Pattern

        System.out.print("  ");
        for (int m = 1; m <= 3; m++) {
            System.out.print('\u2665');
        }
        System.out.print("    ");
        for (int m = 1; m <= 3; m++) {
            System.out.print('\u2665');
        }
        System.out.println();

        System.out.print(' ');
        for (int m = 1; m <= 5; m++) {
            System.out.print('\u2665');
        }
        System.out.print(",,");
        for (int m = 1; m <= 5; m++) {
            System.out.print('\u2665');
        }
        System.out.println();


        // for loop to print the lower part of Heart
        for (int i = 0; i < 14; i++){
            System.out.print("\u2665");
        }
        System.out.println();
        for (int m = 1; m <= 3; m++)
        {
            for (int n = 1; n < 2*m; n++) {
                System.out.print(' ');
            }
            for (int n = 0; n < 16 - 4 * m; n++) {
                System.out.print("\u2665");
            }
            System.out.print(System.lineSeparator());
        }
        for (int n = 0; n < 6; n++) {
            System.out.print(' ');
        }
        for (int n = 0; n < 2; n++) {
            System.out.print("\u2665");
        }

        System.out.println("\n" + nama2 + " [" + umur2 + "] tahun");

        //Generate random int value from 50 to 100
        int min = 50;
        int max = 100;

        int random_int = (int)Math.floor(Math.random()*(max-min+1)+min);
        System.out.print("Tekan ENTER untuk melihat hasil ramalan...");
        Scanner random = new Scanner(System.in);
        random.nextLine();
        System.out.println("Kecocokan anda dengan pasangan anda adalah :" + String.format("%.2f", random_int/1.1) + "%");
        System.out.println("Terima kasih karena anda telah menggunakan jasa ramalan kami.. ^^v");
    }

}