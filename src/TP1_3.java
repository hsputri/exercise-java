import java.util.Scanner;
public class TP1_3 {
    public static void main(String[] args){

        Scanner input = new Scanner(System.in);
        //jawaban poin a
        System.out.print("Masukkan nama Anda: ");
        String nama = input.nextLine();

        String string1 = "Stok Gudang Input Barang";
        System.out.println(string1);
        for(int i = 0; i <= string1.length(); i++) {
            System.out.printf("#");
        }
        System.out.println("\nSelamat Datang " + nama);

        // jawaban poin b
        System.out.print("Masukkan nama barang yang mau ditambah: ");
        String nama_brg = input.nextLine();

        System.out.print("Masukkan jumlah barang yang mau ditambah: ");
        int jumlah_brg = input.nextInt();

        System.out.print("Masukkan harga beli untuk barang tersebut: ");
        double hrg_beli = input.nextDouble();

        System.out.print("Masukkan harga jual untuk barang tersebut: ");
        double hrg_jual = input.nextDouble();

        //jawaban poin c
        String string2 = "Stok Gudang Rincian Barang";
        System.out.println(string2);
        for(int i = 0; i <= string2.length(); i++) {
            System.out.printf("#");
        }
        System.out.println("\nNama Barang : "+ nama_brg);
        System.out.println("Jumlah Barang : "+ jumlah_brg);
        System.out.printf("Harga Beli : Rp. " + "%.2f",hrg_beli);
        System.out.printf("\nHarga Jual : Rp. " + "%.2f", hrg_jual);
    }
}
