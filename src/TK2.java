import java.util.*;

public class TK2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        char ulang;

        String nama;
        do{
            System.out.print("Masukkan Nama Anda [1...25] :");
            nama = input.nextLine();
            if(nama.length() < 1 || nama.length() > 25){
                System.out.println("jumlah karakter nama min. 1 max. 25");
            }
        } while ( nama.length() < 1 || nama.length() > 25 );

        int nim;
        do{
            System.out.print("Masukkan NIM Anda [Harus 10 karakter] :");
            nim = input.nextInt();
            if(String.valueOf(nim).length() != 10){
                System.out.println("angka yang dimasukkan tidak berjumlah 10 karakter");
            }
        } while ( String.valueOf(nim).length() != 10);

        for(int i = 0; i <= 50; i++) {
            System.out.printf("@");
        }

        System.out.println("\nRegistrasi Sukses.,");
        System.out.println("Selamat datang " + nama + " [NIM : " + nim + "].. ^^v");
        System.out.println("Mari belajar macam-macam deret bilangan..");

        for(int i = 0; i <= 50; i++) {
            System.out.printf("@");
        }

        do {
            int number;
            do {
                System.out.print("\nMasukkan Sembarang Angka [5...20] :");
                number = input.nextInt();
                if(number < 5 || number > 20){
                    System.out.println("angka yang dimasukkan harus antara 5-20");
                }
            } while (number < 5 || number > 20);

            for (int i = 0; i <= 50; i++) {
                System.out.printf("@");
            }

            String string1 = "\nDeret Bilangan";
            System.out.println(string1);
            for (int i = 0; i <= string1.length(); i++) {
                System.out.printf("#");
            }

            System.out.println("\n " + number + " Bilangan Genap :");
            int sum1 = 0;
            for (int a = 2; a <= number * 2; a += 2) {
                System.out.print(a + " ");
                sum1 += a;
            }
            System.out.println("\nHasil Penjumlahan = " + sum1);

            System.out.println("\n " + number + " Bilangan Ganjil :");
            int sum2 = 0;
            for (int a = 1; a <= number * 2; a += 2) {
                System.out.print(a + " ");
                sum2 += a;
            }
            System.out.println("\nHasil Penjumlahan = " + sum2);

            int sum3 = 0;

            int tot_prev = 0, bil1 = 0, bil2 = 1, bil_ulang = 1;


            System.out.print("\n " + number + " Bilangan Genap :");
            System.out.print(tot_prev);

            while(bil_ulang <= number) {
                tot_prev = bil1+bil2;
                bil2 = bil1;
                bil1 = tot_prev;

                System.out.print(" "+tot_prev);
                bil_ulang++;
                sum3 += tot_prev;
            }

            System.out.println("\n Hasil Penjumlahan =" + sum3);

            System.out.print("Anda Ingin Mengulang (y/t)? ");
            ulang = input.next().charAt(0);

            System.out.println();

        } while (ulang!= 't');


    }

}
